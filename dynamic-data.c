#include <stdio.h>
#include <stdlib.h>
#include <math.h>
// global float array
float *input;
// record the num of elements in the array
int numInArray;
// record the capacity of the array
int capacity;
// compare methord for float number
int compare (const void * a, const void * b)
{
  float fa = *(const float*) a;
  float fb = *(const float*) b;
  return (fa > fb) - (fa < fb);
}
// give a float array and its length return its mean
float findMean(float* sample, int length){
    float res = 0.0;
    for(int i =0; i < length; i++){
        res = res + sample[i];
    }
    res = res /(float)length;
    return res;
}
// give a float array and its length return its median
float findMedian(float* sample, int length){
    float res;
    qsort(sample, length, sizeof(float), compare);
    if(length % 2 == 0){
        res = (sample[length/2 - 1] + sample[length/2]) / (float)2;
    } else {
        res = sample[length/2];
    }
    return res;
}
// give a float array and its length return its stddev
float findStddev(float* sample, int length){
    float mean = findMean(sample, length);
    float res = 0.0;
    for(int i =0; i < length; i++){
        res = res + (sample[i] - mean)*(sample[i] - mean);
    }
    res = res / (float)length;
    res = sqrtf(res);
    return res;
}
int main(int argv, char **argc){
    input = malloc(sizeof(float) *20);
    numInArray = 0;
    capacity = 20;
    if(argv < 2){
        printf("Please input a filename\n");
        return 0;
    }
    FILE *in = fopen(argc[1], "r");
    if(in == NULL){
        printf("err!\n");
        return -1;
    }
    
    while(!feof(in)){
        if(fscanf(in, "%f", &input[numInArray])>0){
            //printf("error with scanf a float number\n");
            //return -1;
            numInArray++;
            if(numInArray >= capacity){
               input = realloc(input, 2*capacity*sizeof(float));
               capacity = 2 * capacity;
           }
        }
        
        
        
    }
    
    float mean = findMean(input, numInArray);
    float median = findMedian(input, numInArray);
    float stddev = findStddev(input, numInArray);
    printf("Results\n");
    printf("---------\n");
    printf("Num values:       %d\n", numInArray);
    printf("    mean:         %f\n", mean);
    printf("    median:       %f\n", median);
    printf("    stddev:       %f\n", stddev);
    printf("Unused array capacity: %d\n", capacity - numInArray);
    
    fclose(in);
    free(input);
    return 0;

}